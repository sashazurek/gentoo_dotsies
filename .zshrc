# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/sasha/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source $HOME/.config/aliases
source $HOME/.config/promptline/promptline.sh

# Add ~/.local/bin if exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
# Add ~/.cargo/bin if it exists
if [ -d "$HOME/.cargo/bin" ] ; then
    PATH="$HOME/.cargo/bin:$PATH"
fi
unsetopt BEEP
