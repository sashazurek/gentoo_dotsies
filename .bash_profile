# /etc/skel/.bash_profile

# This file is sourced by bash for login shells.  The following line
# runs your .bashrc and is recommended by the bash info pages.
if [[ -f ~/.bashrc ]] ; then
	. ~/.bashrc
fi

# Import aliases
if [ -f $HOME/.config/aliases ]; then
    . $HOME/.config/aliases
fi

# Import promptline
if [ -f $HOME/.config/promptline/promptline.sh ]; then
    . $HOME/.config/promptline/promptline.sh
fi
