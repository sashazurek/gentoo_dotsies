" automated vim-plug install
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Conditional plugin loading
function! Cond(Cond, ...)
    let opts = get(a:000, 0, {})
    return a:Cond ? opts : extend(opts, { 'on': [], 'for': [] })
endfunction

" vim-plug
call plug#begin('~/.vim/plugged')
    " plugins go here
    if !exists('g:vscode') "
        Plug 'sheerun/vim-polyglot'
        Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin'
        Plug 'neoclide/coc.nvim', {'branch': 'release'}
        "Plug 'junegunn/fzf', { 'do': { -> fzf#install()  }  }
        Plug 'airblade/vim-gitgutter'
        Plug 'tpope/vim-fugitive'
        Plug 'RRethy/vim-hexokinase', { 'do': 'make hexokinase' }
        Plug 'rust-lang/rust.vim'
    endif
    " truncated plugin set, compatible with vscode-neovim
    Plug 'jiangmiao/auto-pairs'
    Plug 'dracula/vim',{ 'as': 'dracula'  }
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'edkolev/promptline.vim'
    Plug 'edkolev/tmuxline.vim'
    Plug 'easymotion/vim-easymotion', Cond(!exists('g:vscode')) 
    Plug 'asvetliakov/vim-easymotion', Cond(exists('g:vscode'), {'as': 'vsc-easymotion'}) 
call plug#end()

" text stuff
set encoding=utf-8
colorscheme dracula
let g:airline_powerline_fonts=1
let g:airline_theme='dracula'
let g:airline#extensions#tabline#enabled = 1
let g:Hexokinase_highlighters = ['backgroundfull']
syntax enable
set relativenumber
set number
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set termguicolors
filetype plugin indent on
set cmdheight=1

" tab settings
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

" tmux title
if exists('$TMUX')
  autocmd BufEnter * call system("tmux rename-window " . expand("%:t"))
  autocmd VimLeavePre * call system("tmux setw automatic-rename")
  autocmd BufEnter * let &titlestring = ' ' . expand("%:t")
  set title
endif

" no auto comment
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" custom binds
cmap w!! w !sudo tee > /dev/null %

" tmuxline settings
" let g:airline#extensions#tmuxline#enabled = 0
